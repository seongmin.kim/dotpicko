package dotpicko

import "encoding/json"

func DecodeResponse(data []byte) (*FetchResponse, error) {
	var response FetchResponse
	var err = json.Unmarshal(data, &response)
	return &response, err
}
