package dotpicko

type DataSource string

const (
	NEWEST  DataSource = "https://api.dotpicko.net/v2/works"
	POPULAR DataSource = "https://api.dotpicko.net/works/trend"
)

type  FetchResponse struct {
	Data struct {
		Works    []Work     `json:"works"`
		NextURL  DataSource `json:"next_url"`
	} `json:"data"`
}

type Work struct {
	ID          uint        `json:"id"`
	User        UserInfo    `json:"user"`
	ImageURL    string      `json:"image_url"`
	OPGImageURL string      `json:"opg_image_url"`
	ShareURL    string      `json:"share_url"`
	Title       string      `json:"title"`
	ColorCodes  []ColorCode `json:"color_codes"`
	LikeCount   uint        `json:"like_count"`
	Width       uint        `json:"width"`
	Height      uint        `json:"height"`
	CreatedAt   int64       `json:"created_at"`
	IsLike      bool        `json:"is_like"`
}

type UserInfo struct {
	ID              uint   `json:"id"`
	Name            string `json:"name"`
	Description     string `json:"text"`
	URL             string `json:"url"`
	ShareURL        string `json:"share_url"`
	ProfileImageURL string `json:"profile_image_url"`
	IsFollowed      bool   `json:"is_followed"`
	FollowedCount   uint32 `json:"followed_count"`
	FollowerCount   uint32 `json:"follower_count"`
}

type ColorCode struct {
	Red   uint8 `json:"red"`
	Green uint8 `json:"green"`
	Blue  uint8 `json:"blue"`
	Alpha uint8 `json:"alpha"`
}
