package dotpicko

import (
	"io"
	"net/http"
	"io/ioutil"
)

func drain(stream io.ReadCloser) ([]byte, error) {
	return ioutil.ReadAll(stream)
}

func fetch(from DataSource) ([]byte, error) {
	var url = string(from)
	var httpResponse, err = http.Get(url)
	if err != nil {
		return nil, err
	}
	defer httpResponse.Body.Close()
	return drain(httpResponse.Body)
	
}

func Fetch(from DataSource) (*FetchResponse, error) {
	var bytes, err = fetch(from)
	if err != nil {
		return nil, err
	}

	return DecodeResponse(bytes)
}

